//////////////
////Katie Shreero
////CSE 2 hw01 Welcome Class
////
public class WelcomeClass {
  
  public static void main(String[] args) {
    ////////prints welcome and design
    System.out.println("  -----------");
    System.out.println("  | WELCOME |");
    System.out.println("  -----------");
    System.out.println("  ^  ^  ^  ^  ^  ^");
    System.out.println(" / \\/ \\/ \\/ \\/ \\/ \\ "); 
    System.out.println("<-K--G--S--3--1--9>");
    System.out.println(" \\ /\\ /\\ /\\ /\\ /\\ /");
    System.out.println("  v  v  v  v  v  v");
     ////// Prints autobiographic statement
    System.out.println("I am currently a junior at Lehigh studying in the IBE program. Fly Eagles Fly.");
  }
}