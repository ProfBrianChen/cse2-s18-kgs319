//Katie Shreero 2/9/17
//CSE 2 Check
///////////////////////
//This program uses the scanner class to obtain the cost of a check,
//the percent tip you wish to pay, and the number of ways a check will be split
//to determine how much each person in  group must pay.
/////
///import scanner that allows user data to be collected
import java.util.Scanner;
public class Check{
    			// main method required for every Java program
   			public static void main(String[] args) {
          //declares instance of scanner object 
          Scanner myScanner = new Scanner( System.in );
          //prompts user to input original cost of check
          System.out.print("Enter the original cost of the check in the form xx.xx: ");
          //accepts user input
          double checkCost = myScanner.nextDouble();
          //prompts user to input tip percentage they wish to pay
          System.out.print("Enter the percentage tip that you wish to pay as a whole number (in the form xx): ");
          //accepts user input
          double tipPercent = myScanner.nextDouble();
          //Convert the percentage into a decimal value
          tipPercent /= 100;
          //prompts user to enter number of ways check will be split
          System.out.print("Enter the number of people who went out to dinner: ");
          //accepts user input
          int numPeople = myScanner.nextInt();
          double totalCost;
          double costPerPerson;
          int dollars,   //whole dollar amount of cost 
          dimes, pennies; //for storing digits to the right of the decimal point for the cost$ 
          totalCost = checkCost * (1 + tipPercent);
          costPerPerson = totalCost / numPeople; //get the whole amount, dropping decimal fraction
          dollars=(int)costPerPerson; 
          //get dimes amount, e.g., 
          // (int)(6.73 * 10) % 10 -> 67 % 10 -> 7
          //  where the % (mod) operator returns the remainder
          //  after the division:   583%100 -> 83, 27%5 -> 2 
          dimes=(int)(costPerPerson * 10) % 10;
          pennies=(int)(costPerPerson * 100) % 10;
          //prints number of dimes and pennies each person should pay
          System.out.println("Each person in the group owes $" + dollars + '.' + dimes + pennies);

          



}  //end of main method   
  	} //end of class