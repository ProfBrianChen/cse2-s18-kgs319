//////////////
////Katie Shreero
////CSE 2 hw02 Arithmetic
////
public class Arithmetic {
  
  //main method required for every Java program
  public static void main(String[] args) {  
    //Number of pairs of pants, integer value
      int numPants = 3;
    //Cost per pair of pants, decimal stored in double
      double pantsPrice = 34.98;
    //Number of shirts
      int numShirts = 2;
    //Cost per shirt
      double shirtPrice = 24.99;
    //Number of belts
      int numBelts = 1;
    //cost per belt
      double beltCost = 33.99;
    //the tax rate
      double paSalesTax = 0.06;
        double TotalCostOfPants,TotalCostOfShirts,TotalCostOfBelts; //creates total cost variables
        double TotalPretaxCost; //creates pretax cost variable
        double SalesTaxOnPants,SalesTaxOnShirts,SalesTaxOnBelts; //creates sales tax variables
        double TotalSalesTax; //creates total sales tax variables
        double TotalCost; //creates total cost including tax variables
        
    TotalCostOfPants=numPants*pantsPrice; //calculates total cost of pants
    TotalCostOfShirts=numShirts*shirtPrice; //calculates total cost of shirts
    TotalCostOfBelts=numBelts*beltCost; ////calculates total cost of belts
      System.out.println("The pants cost a total of $"+TotalCostOfPants+"."); //prints total cost of pants
      System.out.println("The shirts cost a total of $"+TotalCostOfShirts+"."); //prints total cost of shirts
      System.out.println("The belts cost a total of $"+TotalCostOfBelts+"."); //prints total cost of belts
    
    TotalPretaxCost=TotalCostOfPants+TotalCostOfShirts+TotalCostOfBelts; //sum of costs before tax
      System.out.println("The total cost of the purchase before tax is $"+TotalPretaxCost+"."); //print pretax cost
    
    SalesTaxOnPants=TotalCostOfPants*paSalesTax; //calculates sales tax by multiplying cost of pants by tax rate
    SalesTaxOnShirts=TotalCostOfShirts*paSalesTax; //calculates sales tax by multiplying cost of shirts by tax rate
    SalesTaxOnBelts=TotalCostOfBelts*paSalesTax; //calculates sales tax by multiplying cost of belts by tax rate
    
      int SalesTaxOnPantsInt= (int) (SalesTaxOnPants*100); //000 format to store three digits in int
        double  SalesTaxOnPantsFinal= (double) SalesTaxOnPantsInt/100; //converts int to double, save two demical places 0.00
      int SalesTaxOnShirtsInt= (int) (SalesTaxOnShirts*100); //000 format to store three digits in int
         double SalesTaxOnShirtsFinal= (double) SalesTaxOnShirtsInt/100; //converts int to double, save two demical places 0.00
      int SalesTaxOnBeltsInt= (int) (SalesTaxOnBelts*100); //000 format to store three digits in int
         double SalesTaxOnBeltsFinal= (double)SalesTaxOnBeltsInt/100; //converts int to double, save two demical places 0.00
    
    System.out.println("The total sales tax on pants is $"+SalesTaxOnPantsFinal+"."); //prints total sales tax on pants
      System.out.println("The total sales tax on shirts is $"+SalesTaxOnShirtsFinal+"."); //prints total sales tax on shirts
      System.out.println("The total sales tax on belts is $"+SalesTaxOnBeltsFinal+"."); //prints total sales tax on belts
   
    TotalSalesTax=SalesTaxOnPantsFinal+SalesTaxOnShirtsFinal+SalesTaxOnBeltsFinal; //add individual sales taxes
      System.out.println("The total sales tax is $"+TotalSalesTax+"."); //print total sales tax
   
    TotalCost=TotalCostOfPants+TotalCostOfShirts+TotalCostOfBelts+TotalSalesTax; //add all costs
      System.out.println("The total cost of the purchase is $"+TotalCost+"."); //print total cost
  } //end main method
} //end class
    
    
