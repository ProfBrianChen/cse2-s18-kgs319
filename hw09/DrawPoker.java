//Katie Shreero 
//CSE 2 4/17/18 DrawPoker
public class DrawPoker{
   
  public static void main(String [] args) {
    
    int [] deck = new int [52]; //create array for deck of 52 cards 
    
    for (int i = 0; i<52; i++) { //fill deck with numbers 0 to 51
      deck[i] = i;
       
    } //close for loop
    
    
    shuffle(deck); //call method to randomize order
		
		deal(deck); //distribute two hands of 5 cards each
		
		
    

    

    
  } //close main method
  
  //shuffling method
    public static void shuffle(int [] cards){ //no output, array input
        for(int i=0; i<cards.length; i++){ 
            int set= (int) (cards.length* Math.random()); //create random order
            int temp = cards[set]; //put cards into random order
            cards[set] = cards[i];
            cards[i]= temp; //copy and save 
            
        }
        
    } //close shuffle
  
  //dealing method
    public static void deal(int [] cards) {
      int [] hand1 = new int [5]; //create array with 5 cards for hand 1
			int [] hand2 = new int [5]; //array of 5 cards for hand 2
      
			//first card to player 1, second to player 2...
      hand1 [0] = cards [0];
      hand1 [1] = cards [2];
      hand1 [2] = cards [4];
      hand1 [3] = cards [6];
      hand1 [4] = cards [8];
			
			hand2 [0] = cards [1];
      hand2 [1] = cards [3];
      hand2 [2] = cards [5];
      hand2 [3] = cards [7];
      hand2 [4] = cards [9];
      
			//print hand 1
      for ( int i = 0; i< hand1.length; i++) {
				//show modulus 13 to show equal value betweeen suits
        System.out.println(hand1[i]%13 + " ");
      } //close for loop
			System.out.println();
			for (int j = 0; j<hand2.length; j++){
				System.out.println(hand2[j]%13 + " ");
			}
      

        System.out.println();
        System.out.print("pair hand 1: ");
        System.out.println(pair(hand1));
				System.out.print("pair hand 2: ");
        System.out.println(pair(hand2));
        System.out.print("three of a kind hand 1: ");
        System.out.println(threeOfAKind(hand1));
				System.out.print("three of a kind hand 2: ");
        System.out.println(threeOfAKind(hand2));
        System.out.print("flush hand 1: ");
        System.out.println(flush(hand1));
				System.out.print("flush hand 2: ");
        System.out.println(flush(hand2));
        System.out.print("full house hand 1: ");
        System.out.println(fullHouse(hand1));
				System.out.print("full house hand 2: ");
        System.out.println(fullHouse(hand2));
        System.out.print("high card hand 1: ");
        System.out.println(highCard(hand1));
				System.out.print("high card hand 2: ");
        System.out.println(highCard(hand2));

			//definite win if one has full house and other does not
			if(fullHouse(hand1) == true && fullHouse(hand2) == false){ 
            System.out.print("Player 1 wins!");
            
        }
        
			//definite win if one has full house and other does not
        if(fullHouse(hand2) == true && fullHouse(hand1) == false){
            System.out.print("Player 2 wins!");   
        }
			
			//tie if both have full house
        if(fullHouse(hand1) == true && fullHouse(hand2) == true){
            System.out.print("Tie!");
            
        }
        //neither have full house, look for flush 
         if(fullHouse(hand1) == false && fullHouse(hand2) == false){
					 //defninite win if one has flush and other does not 
             if(flush(hand1) == true && flush(hand2) == false){
            System.out.print("Player 1 wins!");
            
        }
        //defninite win if one has flush and other does not 
        if(flush(hand2) == true && flush(hand1) == false){
            System.out.print("Player 2 wins!");
            
        }
				//tie if both have flush
         if(flush(hand1) == true && flush(hand2) == true){
            System.out.print("Tie!");
            
        }
					 //if neither have flush look for three of a kind
        if(flush(hand1) == false && flush(hand2) == false){
					
            if(threeOfAKind(hand1) == true && threeOfAKind(hand2) == false){
            System.out.print("Player 1 wins!");
            
        }
       
        
        if(threeOfAKind(hand2) == true && threeOfAKind(hand1) == false){
            System.out.print("Player 2 wins!");
        }
         if(threeOfAKind(hand1) == true && threeOfAKind(hand2) == true){
            System.out.print("Tie!");
        }
        
         if(threeOfAKind(hand1) == false && threeOfAKind(hand2) == false){
             if(pair(hand1) == true && pair(hand2) ==false) { //player 1 has pair, player 2 does not
            System.out.print("Player 1 wins!");
            
        }
            
         if(pair(hand2) == true && pair(hand1) ==false) {//player 2 has pair, player 1 does not
            System.out.print("Player 2 wins!");
            
        }
        if(pair(hand1) == true && pair(hand2) ==true) { //both have pair
            System.out.print("Tie!");
            
        }
        
        
        if(pair(hand1) == false && pair(hand2) ==false){ //if no pair, compare high card
            if(highCard(hand1) > highCard(hand2)){
            System.out.print("Player 1 wins!");
        }
        if(highCard(hand1) < highCard(hand2)){
            System.out.print("Player 2 wins!");
        }
		     }
            
        }
            
        }
     
    }

			System.out.println();
      
    } //close deal

  
  public static boolean pair (int [] cards) {
    
		boolean hasPair = false;
		//looking at value of card some use %. Use placement in array to compare
		if(cards[0]%13 == cards[1]%13 || cards[0]%13 == cards[2]%13 || cards[0]%13 == cards[3]%13 || cards[0]%13 == cards[4]%13 || 
			 cards[1]%13 == cards[2]%13 || cards[1]%13 == cards[3]%13 || cards[1]%13 == cards[4]%13 ||
			 cards[2]%13 == cards[3]%13 || cards[2]%13 == cards[4]%13 ||
			 cards[3]%13 == cards[4]%13) {
			hasPair = true;
		}
		else { 
			hasPair = false;
		}
    return hasPair;
		
  } //close pair

	public static boolean threeOfAKind (int [] cards) {
		boolean hasThreeOfAKind = false;
		if(cards[0]%13 == cards[1]%13 && cards[1]%13 == cards[2]%13){
			hasThreeOfAKind = true;
		}
		if(cards[1]%13 == cards[2]%13 && cards[2]%13 == cards[3]%13){
			hasThreeOfAKind = true;
		}
		if(cards[2]%13 == cards[3]%13 && cards[3]%13 == cards[4]%13){
            hasThreeOfAKind= true;
        }
		return hasThreeOfAKind;

	}
	
	//flush method
	public static boolean flush(int [] cards) {
		  boolean hasFlush= false;
			//5 of same suit so use division to compare
      if(cards[0]/13!=0 && cards[1]/13!=0 && cards[2]/13!=0 && cards[3]/13!=0 && cards[4]/13!=0){
            hasFlush= true;
        }
        return hasFlush;

	}
	
	//full house method
	public static boolean fullHouse(int [] cards){
		boolean hasFullHouse= false;
		if(((cards[0]%13 == cards[1]%13 
				 	&& cards[1]%13 == cards[2]%13)) 
			 		&& cards[3]%13 == cards[4]%13 
			 		&& (cards[0]%13!= 0) 
			 		&& (cards[2]%13!=0)) {
				hasFullHouse= true;
		}
		
		else if(((cards[0]%13 == cards[1]%13) && 
						((cards[2]%13 == cards[3]%13 && 
							cards[3]%13 == cards[4]%13)) && 
						 (cards[0]%13!=0) && 
						 (cards[2]%13!=0))) {
				hasFullHouse=true;
		}
		return hasFullHouse;
    } //close fullHouse

	//high card method
		public static int highCard(int [] cards){
			int high=0;
			for(int i=0; i<cards.length; i++){
					if(cards[i]%13> high){
							high=cards[i]%13; 

					}
			}
	return high;
    }

	
	
  
}