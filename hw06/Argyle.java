//Katie Shreero 3/20/18
//CSE 2 Argyle
//Use loops to print an argyle pattern; prompt user for length and width of dipslay, the length of each side of the diamond
//(all equal), the width of the center stripe (thickness of the diamond), and the three characters that will make up the pattern


//could not complete. data validation should work as should printing  with varying stripe width

import java.util.Scanner;
public class Argyle {
  //main method required for all Java programs
  public static void main(String[] args) {
    
    //declare scanner
    Scanner myScanner = new Scanner(System.in);
    
    
    boolean widthValid = false;
		int width = 0;
    
    //check to validate that width is a positive integer
    while(widthValid == false)  {
			System.out.print("Enter a positive integer for the number of characters in the width of the viewing window: ");
			width = myScanner.nextInt();
			//if conditions are met, exit loop
     if(width > 0) {
			 widthValid = true;
		 }
			//else print error
			else {
			System.out.println("Error: The number entered is invalid.");
			System.out.println();
			}
     }
   
		
		
		
		boolean heightValid = false;
		int height = 0;
    
    //check to validate that height is a positive integer
    while(heightValid == false)  {
			System.out.print("Enter a positive integer for the number of characters in the height of the viewing window: ");
			height = myScanner.nextInt();
			//if conditions are met, exit loop
     if(height > 0) {
			 heightValid = true;
		 }
			//else print error
			else {
			System.out.println("Error: The number entered is invalid.");
			System.out.println();
			}
     }
		
		
		
    boolean sideValid = false;
		int side = 0;
    
    //check to validate that width of diamond is a positive integer
    while(sideValid == false)  {
			System.out.print("Enter a positive integer for the width of the argyle diamond: ");
			side = myScanner.nextInt();
     //if conditions are met, exit loop 
			if(side > 0) {
			 sideValid = true;
		 }
			//else print error
			else {
			System.out.println("Error: The number entered is invalid.");
			System.out.println();
			}
     }
		
		
		
    boolean stripeValid = false;
		int stripe = 0;
    
    //check to validate that stripe width is a positive, odd integer, and is no larger than half of the wide of the diamond width
    while(stripeValid == false)  {
			System.out.print("Enter a positive, odd integer for the width of the argyle stripe: ");
			stripe = myScanner.nextInt();
     //if conditions are met, exit loop 
			if(stripe > 0 && stripe%2 == 1 && stripe<(2*side)) {
			 stripeValid = true;
		 }
			//else print error
			else {
			System.out.println("Error: The number entered is invalid.");
			System.out.println();
			}
     }
		
		//collect chracters, scanned as strings 
    System.out.print("Enter a first character for the pattern fill: ");
    String temp1 = myScanner.next();
		char char1 = temp1.charAt(0);

		System.out.print("Enter a second character for the pattern fill: ");
    String temp2 = myScanner.next();
		char char2 = temp2.charAt(0);
   
    System.out.print("Enter a third character for the stripe fill: ");
    String temp3 = myScanner.next();
		char char3 = temp3.charAt(0); 
    

		int diameter = side*2;
		int stripePos = 0;
		int c = 0;
   
		//repeat for entered height number of rows
		for (int i=0; i < height; i++){
		//repeat for entered width number of columns across 
			for (int j=0; j < width; j++) {
			//for second half of x shape, placement of stripe character is opposite
				if(j > diameter){
				 c = j - diameter - 1;
			 }
				//for first half of x shape
			 else{
				 c = j;
			 }
				//print stripe character when row corresponds to column
			 if(c == stripePos || c == (diameter - stripePos -1)){
					 for(int stripeWid = 0; stripeWid < stripe ; stripeWid++){
					 System.out.print(char3);
						 
					 }
			
				 }
				//else print char 1
				 else{
					 System.out.print(char1);
				 }
			 
		 }
		 stripePos++;
		 if(diameter ==height){
			 stripePos = 0;
		 }
		 System.out.println(); 
	 }
    

  }
}