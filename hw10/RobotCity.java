//Katie Shreero 4/24.18
//CSE 2 RobotCity
import java.util.Random; 

public class RobotCity{
  
  // main method
  public static void main(String[] args){
    Random rand = new Random(); 
    int[][] cityArray = buildCity(); // save array into cityArray
    display(cityArray); //go to method to print out the city
    int k = rand.nextInt(cityArray.length*cityArray[0].length); //generate random number of robots
    invade(cityArray, k); // method to invade city
    display(cityArray); //method to display city after the invasion
    
    for(int i = 0; i < 5; i++){ //loop 5 times
      update(cityArray); //update the city invasion as it moves east-wards
      display(cityArray); //disply the updated city
    }
  }
  
  //method to create city
  public static int[][] buildCity(){
    Random rand = new Random();
    int x = rand.nextInt(5) + 10; //x coord
    int y = rand.nextInt(5) + 10;  //y coord
    
    int city[][] = new int[x][y]; //array for city of random dimensions
    
    // create population for each block
    for(int i = 0; i < x; i++){ //y coord
      for(int j = 0; j < y; j++){ //x coord
        int pop = rand.nextInt(899) + 100; //Random Population (between 100 - 999);
        city[i][j] = pop; //assign random value to block
      }
    }
    return city; //return the generated city
  }
  
  
  //method to print city
  public static void display(int[][] city){
    for(int i = 0; i < city.length; i++){ //y Coordinate
      for( int j = 0; j < city[0].length; j++){ //x Coordinate
        System.out.printf("%5s ", city[i][j]); //print out current block
      }
      System.out.println(); //new line
    }
    System.out.println(); //new line
  }
  
  //method to invade city
  public static void invade(int[][] city, int k){
    Random rand = new Random();
 
    int i = 0;
    while(i < k){ //loop until all robots are placed on map to attack
      int y = rand.nextInt(city.length); //create random y coord
      int x = rand.nextInt(city[0].length); //create random x coord
      if(city[y][x] < 0){ //if the random coordinate is zero, that means it has already been attacked
        continue; //loop again and create a dif random coordinate; don't increase value of i
      }
      city[y][x] = -city[y][x]; //if random coordinate hasn't already been attacked, attack it by making is negative
      i ++; //increase i by 1
      
    }
  }
  
  //method to update city
  public static void update(int[][] city){
    
    for(int i = 0; i < city.length; i++){ //iterate top to bottom
      for(int j = city[0].length - 1; j >= 0; j--){ //iterate left to right
        if(city[i][j] < 0){ //if pos skip, if negative move robot to the left
          city[i][j] = -city[i][j]; //make current coord pos
          if(city[0].length - 1 == j){ //if at the end of the city block, continue bc these robots disappear
            continue;
          }
          else{ //if not at end of city
            city[i][j+1] = -city[i][j+1]; //make the value to the right of current coord pos
          }
        }
      }
    }
  }
}