/// Katie Shreero 2/2/18
/// CSE 2 Cyclometer
////////////////
///This program should act as a bicycle cylometer
///It should record time and front wheel rotation during that time
///From that we should find the length and distance of two trips
public class Cyclometer {
    	// main method required for every Java program
   	public static void main(String[] args) {
        int secsTrip1=480;    //creates variable to store length in seconds of trip 1
        int secsTrip2=3220;   //creates variable to store length in second of trip 2
        int countsTrip1=1561;  //creates variable to store number of wheel rotations in trip 1
        int countsTrip2=9037;  //creates variable to store number of wheel rotations in trip 2
            //stores intermediate variables and output data
            double wheelDiameter=27.0,  //stores diameter of wheel to convert rotations to distance
            PI=3.14159, //stores value of pi for conversion from count to distance 
            feetPerMile=5280,  //stores number of feet in a mile, use for conversion
            inchesPerFoot=12,   //stores number of inches in a foot, use for conversion
  	        secondsPerMinute=60;  //stores number of seconds in a minute, use for conversion
            double distanceTrip1, distanceTrip2,totalDistance;  //stores variable names for use in later functions
                  System.out.println("Trip 1 took "+ //prints "Trip 1 took"
                            (secsTrip1/secondsPerMinute)+" minutes and had "+ //calculates time in minutes of trip 1 and print value, prints "minutes and had"
                                      countsTrip1+" counts."); //prints rotations of wheel (counts) from trip 1 and prints "counts"
	                System.out.println("Trip 2 took "+ //prints "Trip 2 took"
       	                    (secsTrip2/secondsPerMinute)+" minutes and had "+ //calculates time in minutes of trip 2 and print value, prints "minutes and had"
       	                              countsTrip2+" counts."); //prints rotations of wheel (counts) from trip 2 and prints "counts"
      	           
                  distanceTrip1=countsTrip1*wheelDiameter*PI; //calculates distance in inches using stored count information
                                                              //multiply rotations of tire by the diameter of the wheel by pi
                                                              //for each rotation, the wheel travels a distance equal to the diameter times pi
    	              
	                distanceTrip1/=inchesPerFoot*feetPerMile; // Gives distance in miles
                  distanceTrip2=countsTrip2*wheelDiameter*PI/inchesPerFoot/feetPerMile; //combined calculations for trip 2
	                totalDistance=distanceTrip1+distanceTrip2; //total distance equals sum of trip 1 and trip 2 distances
                      //Print out the output data.
                          System.out.println("Trip 1 was "+distanceTrip1+" miles");
	                        System.out.println("Trip 2 was "+distanceTrip2+" miles");
	                        System.out.println("The total distance was "+totalDistance+" miles");
 



	}  //end of main method   
} //end of class
