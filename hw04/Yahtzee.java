//Katie Shreero 2/20/18
//CSE 2 Yahtzee
///////////////////////
import java.util.Scanner;
import java.util.Random;
public class Yahtzee{
    			// main method required for every Java program
   			public static void main(String[] args) {
          //declares instance of scanner object 
          Scanner myScanner = new Scanner( System.in );
          //prints choice between random roll and user inputed roll
          System.out.print("To roll randomly, type 1 to select your own 5 digit roll, type 2 : ");
          //accepts user input
          int userInput = myScanner.nextInt();
          int d1 = 0;
          int d2 = 0;
          int d3 = 0;
          int d4 = 0;
          int d5 = 0;
       
       //create if statement to only accept value of 1 or 2 from user
       if ( (userInput == 1) || (userInput == 2) ){  
          //create random roll if input is 1
            if (userInput == 1){
              //create variables for each die, generate random number, and print
               d1 = (int) (Math.random()*6)+1;
               d2 = (int) (Math.random()*6)+1;
               d3 = (int) (Math.random()*6)+1;
               d4 = (int) (Math.random()*6)+1;
               d5 = (int) (Math.random()*6)+1;
              System.out.println("Die 1 is " +d1);
              System.out.println("Die 2 is " +d2);
              System.out.println("Die 3 is " +d3);
              System.out.println("Die 4 is " +d4);
              System.out.println("Die 5 is " +d5);
            }

          
            //prompts user to input the value of each die if input is 2
            if (userInput == 2){
                System.out.print("Enter an integer value between 1 and 6 for die 1 : ");
                  
                  //accepts user input for die 1 value
                  d1 = myScanner.nextInt();
                     
                      //rejects input if not integer between 1 and 6 for die 1
                      if ( 
                          (d1 == 1) || (d1 == 2) || (d1 == 3) || (d1 == 4) || (d1 == 5)||(d1 == 6)){
                          }
                       else {
                           System.out.println("Please enter a valid integer");
                            return;
                          } 
                      System.out.print("Enter an integer value between 1 and 6 for die 2 : ");
                   
              
                  //accepts user input for die 2 value
                  d2 = myScanner.nextInt();
                           
                        //rejects input if not integer between 1 and 6 for die 2
                        if ( 
                            (d2 == 1) || (d2 == 2) || (d2 == 3) || (d2 == 4) || (d2 == 5)||(d2 == 6)){
                             }
                          else {
                               System.out.println("Please enter a valid integer");
                                return;
                              }
                      System.out.print("Enter an integer value between 1 and 6 for die 3 : ");

                  //accepts user input for die 3 value
                  d3 = myScanner.nextInt();
                           
                        //rejects input if not integer between 1 and 6 for die 3
                        if ( 
                            (d3 == 1) || (d3 == 2) || (d3 == 3) || (d3 == 4) || (d3 == 5)||(d3 == 6)){
                             }
                         else {
                               System.out.println("Please enter a valid integer");
                                return;
                              }
                      System.out.print("Enter an integer value between 1 and 6 for die 4 : ");

                  //accepts user input for die 4 value
                  d4 = myScanner.nextInt();
                           
                          //rejects input if not integer between 1 and 6 for die 4
                          if ( 
                             (d4 == 1) || (d4 == 2) || (d4 == 3) || (d4 == 4) || (d4 == 5)||(d4 == 6)){
                              }
                           else {
                               System.out.println("Please enter a valid integer");
                                return;
                              }
                      System.out.print("Enter an integer value between 1 and 6 for die 5 : ");

                  //accepts user input for die 5 value
                  d5 = myScanner.nextInt();
                           
                           //rejects input if not integer between 1 and 6 for die 5
                           if ( 
                             (d5 == 1) || (d5 == 2) || (d5 == 3) || (d5 == 4) || (d5 == 5)||(d5 == 6)){
                              }
                           else {
                               System.out.println("Please enter a valid integer");
                                return;
                              } 
                 //prints final inputed roll        
                System.out.println("Your roll is: " + d1 + " " + d2 + " " + d3 + " " + d4 + " " + d5); 
                }
             }
          
          //prints error if user inputs anything other than a 1 or 2
          else {
            System.out.println("Please enter a valid input");
            return;
                }
 
              
          
          
          // Create variables for scoring
            int aces = 0;
            int twos = 0;
            int threes = 0;
            int fours = 0;
            int fives = 0;
            int sixes = 0;
          
            int valueOfAces;
            int valueOfTwos;
            int valueOfThrees;
            int valueOfFours;
            int valueOfFives;
            int valueOfSixes;
            int totalValue;
            
            int totalUpperScore = 0;
            int totalUpperBonus = 0;
          
            int chance = 0;
            int yahtzee = 0;
            int largeStraight = 0;
            int smallStraight = 0;
            int fullHouse = 0;
            int fourKind = 0;
            int threeKind = 0;
          
            int totalLowerScore = 0;
            
            int grandTotal = 0;
          
            //Upper section score
          
            //switch die 1 roll output to count of value 
              switch (d1) {
                    case 1 : 
                      aces++;
                        break;
                   case 2 :
                      twos++;
                        break;
                   case 3 :
                        threes++;
                        break;
                   case 4 :
                       fours++;
                        break;
                   case 5 :
                       fives++;
                        break;
                    case 6 :
                       sixes++;
                        break;
                   }
             
          //switch die 2 roll output to count of value 
              switch (d2) {
                        case 1 : 
                          aces++;
                            break;
                       case 2 :
                          twos++;
                            break;
                       case 3 :
                            threes++;
                            break;
                       case 4 :
                           fours++;
                            break;
                       case 5 :
                           fives++;
                            break;
                        case 6 :
                           sixes++;
                            break;
                     }
             
          //switch die 3 roll output to count of value 
              switch (d3){
                        case 1 : 
                          aces++;
                            break;
                       case 2 :
                          twos++;
                            break;
                       case 3 :
                            threes++;
                            break;
                       case 4 :
                           fours++;
                            break;
                       case 5 :
                           fives++;
                            break;
                        case 6 :
                           sixes++;
                            break;
                  }
             
          //switch die 4 roll output to count of value 
              switch (d4){
                    case 1 : 
                      aces++;
                        break;
                   case 2 :
                      twos++;
                        break;
                   case 3 :
                        threes++;
                        break;
                   case 4 :
                       fours++;
                        break;
                   case 5 :
                       fives++;
                        break;
                    case 6 :
                       sixes++;
                        break;
                  }
           
           //switch die 5 roll output to count of value 
              switch (d5){
                    case 1 : 
                      aces++;
                        break;
                   case 2 :
                      twos++;
                        break;
                   case 3 :
                        threes++;
                        break;
                   case 4 :
                       fours++;
                        break;
                   case 5 :
                       fives++;
                        break;
                    case 6 :
                       sixes++;
                        break;
                }

          
          //calculate value from count for scoring
          valueOfAces = 1 * aces;
          valueOfTwos = 2 * twos;
          valueOfThrees = 3 * threes;
          valueOfFours = 4 * fours;
          valueOfFives = 5 * fives;
          valueOfSixes = 6 * sixes;
         //total value of roll is equal to sum of individual values
          totalValue = (valueOfAces + valueOfTwos + valueOfThrees + valueOfFours + valueOfFives + valueOfSixes);
         
          //Calculate total upper score from sum of values of each rolled number
          totalUpperScore = totalValue;
            //Print initial total score of upper section
            System.out.println("The initial total score of the upper section is " +totalUpperScore);
          
          
          //Give bonus if score from upper section is greater than or equal to 63
          if (totalUpperScore >= 63){
            totalUpperBonus = totalUpperScore + 35;
                }
          //No bonus if score from upper section is less than 63
          if (totalUpperScore < 63){
              totalUpperBonus = 0;
               }
            
          //Print bonus
          System.out.println("The bonus from the upper section is "
                                +totalUpperBonus);
          
         
          //Lower section score
          
          //want highest possible score from roll, so rank else ifs from highest scoring to lowest
          
          //If all dice are of one value, player gets 50 points
          if ((sixes == 5) || (fives == 5) || (fours == 5) || (threes == 5) || (twos == 5) || (aces == 5)) {
              yahtzee = 50;
          }
            //If not a yahtzee, check if series of five
          else if ( 
                         ( (sixes == 1) && (fives ==1) && (fours ==1) && (threes ==1) && (twos ==1) )
                       || ( (fives ==1) && (fours ==1) && (threes ==1) && (twos ==1) && (aces ==1) ) 
                   ){
                         largeStraight = 40;
                   }
            //If not series of five,check if series of four
          else if ( 
                             ( (sixes == 2) && (fives == 1) && (fours == 1) && (threes == 1) )
                          || ( (sixes == 1) && (fives == 2) && (fours == 1) && (threes == 1) )
                          || ( (sixes == 1) && (fives == 1) && (fours == 2) && (threes == 1) )
                          || ( (sixes == 1) && (fives == 1) && (fours == 1) && (threes == 2) )
                          || ( (fives == 2) && (fours == 1) && (threes == 1) && (twos == 1) )
                          || ( (fives == 1) && (fours == 2) && (threes == 1) && (twos == 1) )
                          || ( (fives == 1) && (fours == 1) && (threes == 2) && (twos == 1) )
                          || ( (fives == 1) && (fours == 1) && (threes == 1) && (twos == 2) )
                          || ( (fours == 2) && (threes == 1) && (twos == 1) && (aces == 1) )
                          || ( (fours == 1) && (threes == 2) && (twos == 1) && (aces == 1) )
                          || ( (fours == 1) && (threes == 1) && (twos == 2) && (aces == 1) )
                          || ( (fours == 1) && (threes == 1) && (twos == 1) && (aces == 2) )
                      ){
                          smallStraight = 30;
                     }
             //If not series of four, check if full house
          else if (
                              ( (sixes == 3) && (fives == 2) )
                           || ( (sixes == 3) && (fours == 2) )
                           || ( (sixes == 3) && (threes == 2) )
                           || ( (sixes == 3) && (twos == 2) )
                           || ( (sixes == 3) && (aces == 2) )
                           || ( (fives == 3) && (sixes == 2) )
                           || ( (fives == 3) && (fours == 2) )
                           || ( (fives == 3) && (threes == 2) )
                           || ( (fives == 3) && (twos == 2) )
                           || ( (fives == 3) && (aces == 2) )
                           || ( (fours == 3) && (sixes == 2) )
                           || ( (fours == 3) && (fives == 2) )
                           || ( (fours == 3) && (threes == 2) )
                           || ( (fours == 3) && (twos == 2) )
                           || ( (fours == 3) && (aces == 2) )
                           || ( (threes == 3) && (sixes == 2) )
                           || ( (threes == 3) && (fives == 2) )
                           || ( (threes == 3) && (fours == 2) )
                           || ( (threes == 3) && (twos == 2) )
                           || ( (threes == 3) && (aces == 2) )
                           || ( (twos == 3) && (sixes == 2) )
                           || ( (twos == 3) && (fives == 2) )
                           || ( (twos == 3) && (fours == 2) )
                           || ( (twos == 3) && (threes == 2) )
                           || ( (twos == 3) && (aces == 2) )
                           || ( (aces == 3) && (sixes == 2) )
                           || ( (aces == 3) && (fives == 2) )
                           || ( (aces == 3) && (fours == 2) )
                           || ( (aces == 3) && (threes == 2) )
                           || ( (aces == 3) && (twos == 2) )
                         ){
                             fullHouse = 25;
                         }
            
          //If not full house, check if four of a kind
          else if ( 
                          (aces == 4) || (twos == 4) || (threes == 4) || (fours == 4) || (fives == 4) || (sixes ==4) ) {
                             fourKind = totalValue;
                          }
             
          //If not four of a kind, check if three of a kind
          else if (
                           (aces == 3) || (twos == 3) || (threes == 3) || (fours == 3) || (fives == 3) || (sixes ==3) ) {
                              threeKind =totalValue;
                         }
          //If nothing else, use chance score
          else {
                  chance = totalValue;
                   }
                        
         //value of group is 0 until riteria is met, so total lower score is sum of values of each group
          totalLowerScore= yahtzee + chance + largeStraight + smallStraight + fullHouse + fourKind + threeKind;
            System.out.println("The total score from the lower section is " +totalLowerScore);
          //grand total is the total upper score plus the bonus if any plus the total lowr score
          grandTotal = totalUpperScore + totalUpperBonus + totalLowerScore;
            System.out.println("The grand total score is " +grandTotal);
          
          
          
        }
}