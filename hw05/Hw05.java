//Katie Shreero 3/6/18
//CSE 2 hw05 
//In this homework, we will utilize while loops to accept only certain types of input from a user

import java.util.Scanner;
public class Hw05 {
  //main method required for all Java programs
  public static void main(String[] args) {
    //declares the instance of a scanner
    Scanner myScanner = new Scanner(System.in);
    
    //create boolean true or false for each variable which will be used in loop
    boolean courseNumber = false;
    //create variable for storage of entered value using proper type for each
    int courseNumberInt;
    
    boolean deptName = false;
    String deptNameString;
    
    boolean meetings = false;
    int meetingsInt;
    
    boolean time = false;
    String timeString;
    
    boolean instructor = false;
    String instructorString;
    
    boolean students = false;
    int studentsInt;
    
    //create while loop which will now be entered automatically
    while (courseNumber == false) {
      //prompt user to enter value
      System.out.print("Enter your course number: ");
      //check if entered value is the correct type
      courseNumber = myScanner.hasNextInt();
      //if correct type
      if (courseNumber == true) {
        //store entered value in varible
        courseNumberInt = myScanner.nextInt();
        //print entered value
        System.out.println("Course number: " + courseNumberInt);
        //print new line for ease of read
        System.out.println();
      }
      //if incorrect type entered
      else if (courseNumber == false){
        //create variable to store bad value which will never be printed
        String bad = myScanner.next();
        //print error message
        System.out.println("Error: course number must be an integer");
      }
    }
    
    //process is repeated for each field of interest for either integers or strings
    
    
     while (deptName == false) {
      System.out.print("Enter the name of the department: ");
      deptName = myScanner.hasNext();
      if (deptName == true) {
        deptNameString = myScanner.next();
        System.out.println("Department name: " + deptNameString);
        System.out.println();
      }
      else if (deptName == false){
        String bad = myScanner.next();
        System.out.println("Error: department name must be a string");
      }
    }
      while (meetings == false) {
      System.out.print("Enter the number of times this class meets per week: ");
      meetings = myScanner.hasNextInt();
      if (meetings == true) {
        meetingsInt = myScanner.nextInt();
        System.out.println("Number of meetings: " + meetingsInt);
        System.out.println();
      }
      else if (meetings == false){
        String bad = myScanner.next();
        System.out.println("Error: number of meetings must be an integer");
      }
    }
      while (time == false) {
      System.out.print("Enter the time this class meets: ");
      time = myScanner.hasNext();
      if (time == true) {
        timeString = myScanner.next();
        System.out.println("Time: " + timeString);
        System.out.println();
      }
      else if (time == false){
        String bad = myScanner.next();
        System.out.println("Error:time must be a string");
      }
    }
    
      while (instructor == false) {
      System.out.print("Enter the name of the instructor: ");
      instructor = myScanner.hasNext();
      if (instructor == true) {
        instructorString = myScanner.next();
        System.out.println("Instructor: " + instructorString);
        System.out.println();
      }
      else if (instructor == false){
        String bad = myScanner.next();
        System.out.println("Error:instructor name must be a string");
      }
    }
    
      while (students == false) {
      System.out.print("Enter the number of students in the class: ");
      students = myScanner.hasNextInt();
      if (students == true) {
        studentsInt = myScanner.nextInt();
        System.out.println("Number of students: " + studentsInt);
        System.out.println();
      }
      else if (students == false){
        String bad = myScanner.next();
        System.out.println("Error: number of students must be an integer");
      }
    }
    
    
    
    
    
  }
}