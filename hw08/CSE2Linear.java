//Katie Shreero 4/11/18
//CSE 002 HW08 CSE2Linear

import java.util.Scanner;
import java.util.Random;

//create class for entire code
public class CSE2Linear {
  //create main method
  public static void main(String[] args){
    

    int [] finalGrades;
    
    //space for 15 entries in array
    finalGrades = new int[15];

       System.out.println("Enter 15 ascending ints for final grades in CSE2: ");
      
    //validation for is integer and in range
      for(int i = 0; i < finalGrades.length ; i++) {
        Scanner myScanner = new Scanner(System.in);
        
        int test = myScanner.nextInt();
        
        if (true) {
                  
          if (test < 100 && test > 0) {
            int num = test;
              finalGrades[i] = num;
          }
          else {
            System.out.println("out of bounds");
            i--;
          }
        }
        else {
          System.out.println("Please enter a valid integer.");
          i--;
        }
          
  
        
      }
      
      
      
    for(int i = 0;i < finalGrades.length;i++){
      System.out.print(" "+finalGrades[i]);
    }  
    System.out.println();
    
  
  System.out.print("Enter a grade to search for: ");
  Scanner myScanner2 = new Scanner(System.in);
  int gradeCheck = myScanner2.nextInt();
  System.out.println(binarySearch(finalGrades, gradeCheck));
    
  }
  
  //binary search method
  public static boolean binarySearch (int[] finalGrades, int gradeCheck) {
    int low = 0;
    int high = finalGrades.length;

    while(high >= low) {
      //find middle index
      int middle = (low+high)/2;
      //if middle index of finalGrades array is equal to entered check, return true
      if (finalGrades[middle] == gradeCheck){
        System.out.println(gradeCheck + "found at" +(middle+1));
        return true;
       
      }
      //if index is below check, add one to middle 
      if (finalGrades[middle] < gradeCheck) {
        low = middle +1;
      }
      //if index is above check, subtract one from middle       
      if (finalGrades[middle] > gradeCheck) {
        low = middle -1;
      }
    }
  
     
     System.out.println(gradeCheck +" is not in this list.");
    return false;
   
  }
  
  //rearrange array into random order
  public static void randomOrder (int [] finalGrades) {
          
        for(int i=0; i < finalGrades.length; i++) {
          int set = (int) (finalGrades.length * Math.random()); //creates a random integer and sets it to set
          //switch location in array to set
          int temp = finalGrades[set];
          finalGrades[set] = finalGrades[i];
          finalGrades[i]= temp;

          }
    
  }
  
   public static boolean linear( int[] finalGrades, int gradeCheck ){ 
        for(int i = 0; i < finalGrades.length; i++ ){
        if( finalGrades[i] == gradeCheck ){
        gradeCheck++;
        }
        }
        return false;
        }
  
  
  
  

}
