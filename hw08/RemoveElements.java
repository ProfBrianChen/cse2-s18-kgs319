//Katie Shreero 

//take an array and add or remove certain elements
//import scanner
import java.util.Scanner;
//import random generator
import java.util.Random;
//define class
public class RemoveElements{
//define main method
public static void main(String [] arg){
Scanner scan=new Scanner(System.in);
int num[]=new int[10];
int newArray1[];
int newArray2[];
int index,target;
String answer="";
do{
System.out.print("Random input 10 ints [0-9]");
num = randomInput(num);
String out = "The original array is: ";
out += listArray(num);
System.out.println(out);

System.out.print("Enter the index(9) ");
index = scan.nextInt();
newArray1 = delete(num,index);
String out1="The output array is ";
out1 += listArray(newArray1); //return a string of the form &quot;{2, 3, -9}&quot;
System.out.println(out1);


System.out.print("Enter the target value ");
target = scan.nextInt();
newArray2 = remove(num,target);
String out2="The output array is ";
out2+=listArray(newArray2); //return a string of the form &quot;{2, 3, -9}&quot;
System.out.println(out2);

System.out.print("Go again? Enter 'y' or 'Y', anything else to quit");
answer=scan.next();
}while(answer.equals("Y") || answer.equals("y"));
}

//define method to list array
public static String listArray(int num[]){
String out="{";
for(int j=0;j<num.length;j++){
if(j>0){
out+=",";
}
out+=num[j];
}
out+="}";
return out;
}

//define method for filling array
public static int[] randomInput(int [] list){
Random randomGenerator = new Random();

for (int i = 0; i<list.length; i++){
int number= randomGenerator.nextInt(10); //generates random number between 0 and 9
list[i] = number; //fills array
}
return list;
}

//define method for deleting position of array
  public static int [] delete(int[] list,int pos){
    int [] array2= new int[9]; //create new array of length one less than list
    for(int i=0; i<list.length; i++){
    if(i!= pos){ //skip elements at position inputed
    array2[i]=list[i];
    }
    }


    return array2;
    }

//define method for removing inputed members
public static int [] remove(int [] list,int target){
int count=0;
for(int i=0; i<list.length; i++){ //takes counter and counts how many times the target int shows up in the array
if(list[i]==target){
count++;
}
}

int [] array3= new int[(list.length -count)]; //creates array length of list minus the number of target ints in the array
for(int j=0; j <array3.length; j++){
if(list[j]!=target){ //skips over target int
array3[j]=list[j];
}
if(list[j]==target){

}

}


return array3;
}

}
