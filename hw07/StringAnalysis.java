//Katie Shreero
//CSE 2 StringAnalysis
//

import java.util.Scanner;
public class StringAnalysis {
  public static void main(String[] args) {
    
    
  //declare instance of scanner object
  Scanner myScanner = new Scanner (System.in);
  //prompt for string input
  System.out.println("Please enter a string.");
  //declare string to accept string input from scanner 
  String stuff = myScanner.next();
  //declare variable to accept type of input
  String input = "";
  //boolean for entering loop
  boolean validChoice = false;
    //while loop to check if input is all or int 
    while (!validChoice) {
      System.out.print("Would you like to examine all characters or a given int? Type 'all' or 'int'. ");
      //store reading from scanner in type of input variable
      input = myScanner.next();
      //check if input is all or int
      if(input.equals("all") || input.equals("int")) {
        //if true exit loop 
        validChoice = true;
      }
      //else print error and loop to beginning of while loop
      else {
        System.out.println("Input is invalid. Type 'all' or 'int'.");
      }
    }
  
  boolean output;
  int number =0;
  boolean isInt = false;
    
          //if input is an int
          if(input.equals("int")) {  
            //enter loop for data validation
            while (isInt == false) {
              //prompt for number of ints to be analyzed
              System.out.print("How many characters would you like to analyze? : ");
              //initialize new scanner
              Scanner intScanner = new Scanner(System.in);
              //check to see if entered value is an int
              isInt = intScanner.hasNextInt();
                   //if it is an int                             
                   if (isInt == true){
                     //save value
                       number = intScanner.nextInt();
                   }
                    //if not
                    else if (isInt == false) {
                      //print error and loop in while again
                      System.out.println("Please enter an integer.");
                    }
             }
             //save final output as result of called analyze method                                  
            output = analyze(stuff, number);
            //print final output
            System.out.println("Is your string all letters? " + output);
          }
    
    //if user enters all
    if(input.equals("all")) {
      //call analyze method and save final output
      output = analyze(stuff);
      //print final output
      System.out.println("Is your string all letters? " + output);
      
    }
  
  
  }
    

    
  
  //method to be run when all is inputted by user 
  public static boolean analyze (String stuff) {
    
    boolean result = true;
    //for entire length of string
      for(int i=0; i<stuff.length(); i++) {
        //analyze characte at each location 
        char letter = stuff.charAt(i);
            //if any character is not a letter
            if (!Character.isLetter(letter)) {
              //return false
              result = false;
              
            }

  
    }
    //return result as true or false
    return result;
  }
  
    public static boolean analyze (String stuff, int number) {
    //if the integer entered is greater than the number of characters in the string
    if (number > stuff.length()) {
      //analyze the entire length of the string 
      number = stuff.length();
    }
    boolean result = true;
      //for each location of a character in the string
      for(int i=0; i< number; i++) {
        //save individual characters
        char letter = stuff.charAt(i);
            //if any character is not a letter
            if (!Character.isLetter(letter)) {
              //return false
              result = false;
              
            }

  
    }
    return result;
  }
  
  
}
