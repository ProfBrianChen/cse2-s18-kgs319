//Katie Shreero 3/27/18
//CSE 2 Area
//Find area of 3 different shapes using methods

import java.util.Scanner;
public class Area {
public static void main(String[] args) {

//initialize variable to save output of input method as shape 
String shape=input();
 
  //if the entered shape is circle, run the circle area method
  if (shape.equals("circle")) {
  circleAreaMethod();
  }
  //if the entered shape is triangle, run the triangle area method
  if(shape.equals("triangle")){
    triangleAreaMethod();
  }
  //if the entered shape is rectangle, run the rectangle area method
  if(shape.equals("rectangle")){
    rectangleAreaMethod();
  }
}


  //input method which returns a string output with name of shape
public static String input() {

  //initialize scanner, string called shape, and boolean to check validity of shape input
  Scanner myScanner = new Scanner(System.in);
  boolean shapeValid = false;
  String shape = "";

      //always enter loop
      while (shapeValid == false){
 
          //prompt for shape input
          System.out.println("Please choose a shape. Enter 'circle' 'rectangle' or 'triangle'.");
          //store user input in string
          shape = myScanner.next();

          //if shape is one of three acceptable inputs
          if (shape.equals("rectangle") || (shape.equals("triangle")) || shape.equals("circle") ) {
                      //exit while loop
                      shapeValid = true;
                   }
            //else print error and loop back to beginning of while loop
            else {
               System.out.println("This shape is not acceptable. Please enter 'circle' 'rectangle' or 'triangle'.");
              }
  
        }
    //return string shape with "circle" , "triangle" , or "rectangle" stored in it
     return shape;
}
  
  

 //method to calculate area of circle
public static void circleAreaMethod () {

//initialize variable to store radius
double radius = 0;
//use to check if input is valid in while loop 
boolean radiusValid = false;
  
                       
        //always enter loop     
        while (radiusValid == false) {
            //prompt for user to enter radius
            System.out.print("Enter the radius of the circle: ");
            Scanner radiusScanner = new Scanner(System.in);
            //store whether entered radius is a double
            radiusValid = radiusScanner.hasNextDouble();
            //if double
            if (radiusValid == true) {
                //store double as radius 
                radius = radiusScanner.nextDouble();
               }
            //if not double
            else if (radiusValid == false) {
              //eror and loop back to while
                System.out.println("Please enter a double.");
                    }
           } 
                     
double circleArea = Math.PI * Math.pow(radius, 2);
System.out.println(circleArea);

}
  
 
  
  //method for rectangle 
public static void rectangleAreaMethod () {

  //same validity loop as in circle method but now with two parameters
double length = 0;
double width = 0;
boolean lengthValid = false;
boolean widthValid = false;
  
                       
             
        while (lengthValid == false) {
            System.out.print("Enter the length of the rectangle: ");
            Scanner lengthScanner = new Scanner(System.in);
            lengthValid = lengthScanner.hasNextDouble();
            if (lengthValid == true) {
                length = lengthScanner.nextDouble();
               }
            else if (lengthValid == false) {
                System.out.println("Please enter a double.");
                    }
           } 
          
          while (widthValid == false) {
            System.out.print("Enter the width of the rectangle: ");
            Scanner widthScanner = new Scanner(System.in);
            widthValid = widthScanner.hasNextDouble();
            if (widthValid == true) {
                width = widthScanner.nextDouble();
               }
            else if (widthValid == false) {
                System.out.println("Please enter a double.");
                    }
           }
                      
double rectangleArea = length * width;
System.out.println(rectangleArea);

}

  //method for triangle area
public static void triangleAreaMethod () {

  //same loop as rectangle method
double base = 0;
double height = 0;
boolean baseValid = false;
boolean heightValid = false;
  
                       
             
        while (baseValid == false) {
            System.out.print("Enter the base of the triangle: ");
            Scanner baseScanner = new Scanner(System.in);
            baseValid = baseScanner.hasNextDouble();
            if (baseValid == true) {
                base = baseScanner.nextDouble();
               }
            else if (baseValid == false) {
                System.out.println("Please enter a double.");
                    }
           } 
          
          while (heightValid == false) {
            System.out.print("Enter the height of the triangle: ");
            Scanner heightScanner = new Scanner(System.in);
            heightValid = heightScanner.hasNextDouble();
            if (heightValid == true) {
                height = heightScanner.nextDouble();
               }
            else if (heightValid == false) {
                System.out.println("Please enter a double.");
                    }
           }

double triangleArea = ( (.5) * base) * height;
System.out.println(triangleArea);

}

  
  





}

