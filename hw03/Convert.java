//////////////
////Katie Shreero
////CSE 2 hw03 Convert
////Program accepts user input of acres of rainfall and depth in inches and converts to cubic miles of rain
import java.util.scanner;
public class Convert {
  
  //main method required for every Java program
  public static void main(String[] args) {  
        //declares instance of scanner object to enable user to enter data
        Scanner myScanner = new Scanner( System.in );
        //prompt user to enter the affected area in acres
        System.out.print("Enter area affected by hurricane in acres in the form xx.xx: ");
        //accepts user input in double
        double acres = myScanner.nextDouble();
        //prompts user to enter inches of rainfall in effected area
        System.out.print("Enter number of inches of rainfall in affected area in the form xx.xx: ");
        //accepts user input in double
        double inches = myScanner.nextDouble();
        //convert inches to miles
        double InchesToMiles = inches / 63360;
        //converts acres to square miles
        double miles = acres *.0015625;
        //multiplies square miles by inches to get cubic miles
        double CubicMiles = miles * InchesToMiles;
        //prints cubic miles of rain with verbiage
        System.out.println(+CubicMiles+ " cubic miles of rain fell");
    
  }
}

        
        
        