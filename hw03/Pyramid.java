////////////////
////Katie Shreero
////CSE 2 hw03 Pyramid
////Program accepts user input of dimenisons of pyramid and returns volume of the pyramid
import java.util.Scanner;
public class Pyramid {
  
  //main method required for every Java program
  public static void main(String[] args) {  
        //declares instance of scanner object to enable user to enter data
        Scanner myScanner = new Scanner( System.in );
    //prompts user to enter the length of one side of the square base of the pyramid
        System.out.print("Enter length of one side of square base of pyramid: ");
     //accepts user input in int
        int base = myScanner.nextInt(); 
     //prompts user to enter height of pyramid
        System.out.print("Enter height of pyramid: ");
     //accepts user input in int
        int height = myScanner.nextInt();
     //finds area of base of pyramid
        int AreaOfBase = base * base;
     //finds area of base times height to store as int
        int PreVolume =  AreaOfBase * height;
     //calculates volume of pyramid by dividing by 3 and saving as double
        double Volume = PreVolume/3;
    //prints resulting volume inside pyramid
        System.out.println("The volume inside the pyramid is " +Volume+".");
    
  }
}
       